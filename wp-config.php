<?php

// live site url for detection
$remoteSiteUrl = "westernspirit.fi";

//local or remote variables
if(strpos($_SERVER['HTTP_HOST'], $remoteSiteUrl) !== false) {$environment = "remote";} else { $environment = "local";}

//modify your copy of WordPress to work with any domain name  -http://clickontyler.com/support/a/21/wordpress-liftoff/
$s = empty($_SERVER['HTTPS']) ? '' : ($_SERVER['HTTPS'] == 'on') ? 's' : '';
$port = ($_SERVER['SERVER_PORT'] == '80') ? '' : (":".$_SERVER['SERVER_PORT']);
// koska ei oo /wp/ ni kommentoitu tää versio ja käytetää seuraavaa
//$url = "http$s://" . $_SERVER['HTTP_HOST'] ."/oravastaging" . $port;
$url = "http$s://" . $_SERVER['HTTP_HOST'] . $port;
//$url = "http$s://" . $_SERVER['HTTP_HOST'] ."/wp" . $port;
//$url = "http$s://" . $_SERVER['HTTP_HOST'] . $port;
define('WP_HOME', $url);
define('WP_SITEURL', $url);
unset($s, $port, $url);


/** 
 * WordPressin perusasetukset.
 *
 * Tämä tiedosto sisältää seuraavat asetukset: MySQL-asetukset, Tietokantataulun etuliite,
 * henkilökohtaiset salausavaimet (Secret Keys), WordPressin kieli, ja ABSPATH. Löydät lisätietoja
 * Codex-sivulta {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php}. Saat MySQL-asetukset palveluntarjoajaltasi.
 *
 * Automaattinen wp-config.php-tiedoston luontityökalu käyttää tätä tiedostoa
 * asennuksen yhteydessä. Sinun ei tarvitse käyttää web-asennusta, vaan voit 
 * tallentaa tämän tiedoston nimellä "wp-config.php" ja muokata allaolevia arvoja.
 *
 * @package WordPress
 */


/* 
 * Unified variables 
 */   
//$hostname = 'localhost';  
$chartset = 'utf8';  
$collate = 'utf8_swedish_ci';  
$pl_hooks_php = 'true';  

/* 
 * Check for the current environment 
 */  

if($environment === "remote" ){
  $db_name = 'database';  
  $user_name = 'username'; 
  $password = 'password';  
  $hostname = 'localhost';  
}
else{
  $db_name = 'western-spirit';  
  $user_name = 'root'; 
  $password = 'root';  
  $hostname = 'localhost';  
}

  

// ** MySQL settings - You can get this info from your web host ** //  
/** The name of the database for WordPress */  
define('DB_NAME', $db_name);  
  
/** MySQL database username */  
define('DB_USER', $user_name);  
  
/** MySQL database password */  
define('DB_PASSWORD', $password);  
  
/** MySQL hostname */  
define('DB_HOST', $hostname);  
  
/** Database Charset to use in creating database tables. */  
define('DB_CHARSET', $chartset);  
  
/** The Database Collate type. Don't change this if in doubt. */  
define('DB_COLLATE', $collate);  



/** Pagelines enable PHP in hooks */  
define( 'PL_HOOKS_PHP', $pl_hooks_php);






/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Muuta nämä omiksi uniikeiksi lauseiksi!
 * Voit luoda nämä käyttämällä {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org palvelua}
 * Voit muuttaa nämä koska tahansa. Kaikki käyttäjät joutuvat silloin kirjautumaan uudestaan.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'UT1(Pc>`7+r,y~/] e2&`AG/d ?td63D-#o-d:!ZP}E)VG#/Jw 7d`Dw78`0BbPa');
define('SECURE_AUTH_KEY',  '8?m!<t^Wdhru@0[>r)$R`;=x!,/bY(sdj>o^R8i-bCx`l+~1m|y+wg[4Uk-+|:Y ');
define('LOGGED_IN_KEY',    'X_yk#&p}yqPu`zuQ_wxtj dHY+ie|lJ<6s-Fc5w`==6{E:4<:&4 -P)L8va-guaq');
define('NONCE_KEY',        'j&`P+OeAxtrmU}he? UgFg!*!_e9]F<|x]j5$^>5dvA|&R|sS/1By-}.sB,.>l%:');
define('AUTH_SALT',        '!ShKjJcg.-~xXkU%&D^oUe0F-t/qJ1X@c-R?Ow@hg4!-MxKf0Oe #Z}k/XK{bN)K');
define('SECURE_AUTH_SALT', '%uD^mb+%BTy@HSivWVsEqb[V6_I*8Y 0cb+DNH.97yY@G{I%-|xT>5j{*5Nw7Ezy');
define('LOGGED_IN_SALT',   ')gMV+W5U+Xk)ZI|,9Tu{6Uc+GDpUf$!?ZeR{+ONLByuL`f*e|jmF,XkR8QTsWlw+');
define('NONCE_SALT',       'iJb+*([(`[_.kM-/lr2u}Wqk;/sA7_7!kcp`83E-D%<sVI,!4L>Rqz]W-t`w{rm~');



/**#@-*/

/**
 * WordPressin tietokantataulujen etuliite (Table Prefix).
 *
 * Samassa tietokannassa voi olla useampi WordPress-asennus, jos annat jokaiselle
 * eri tietokantataulujen etuliitteen. Sallittuja merkkejä ovat numerot, kirjaimet
 * ja alaviiva _.
 *
 */
$table_prefix  = 'wp_Vz4ysL_';

/**
 * WordPressin kieli.
 *
 * Muuta tämä WordPressin kieliasetusten muuttamiseksi. Vastaavasti nimetty 
 * kielitiedosto pitää asentaa hakemistoon wp-content/languages. Esimerkiksi,
 * asenna de.mo wp-content/languages -hakemistoon ja muuta WPLANG:in arvoksi 'de'
 * käyttääksesi WordPressiä saksan kielellä.
 */
define ('WPLANG', 'fi');

/**
 * Kehittäjille: WordPressin debug-moodi.
 *
 * Muuta tämän arvoksi true jos haluat nähdä kehityksen ajan debug-ilmoitukset
 * Tämä on erittäin suositeltavaa lisäosien ja teemojen kehittäjille.
 */
define('WP_DEBUG', false);

/* Siinä kaikki, älä jatka pidemmälle! */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
