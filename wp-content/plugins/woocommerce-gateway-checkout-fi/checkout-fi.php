<?php
/**
 * Plugin Name: WooCommerce Checkout (Finland) Payment Gateway
 * Plugin URI: http://www.woothemes.com/products/checkout-fi-gateway/
 * Description: Accept credit cards and money transfers in WooCommerce with the Checkout Gateway
 * Author: SkyVerge
 * Author URI: http://www.skyverge.com
 * Version: 1.2.1
 * Text Domain: woocommerce-gateway-checkout-fi
 * Domain Path: /i18n/languages/
 *
 * Copyright: (c) 2011-2014 SkyVerge, Inc. (info@skyverge.com)
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   WC-Checkout-Fi
 * @author    SkyVerge
 * @category  Payment-Gateways
 * @copyright Copyright (c) 2011-2014, SkyVerge, Inc.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Required functions
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

// Plugin updates
woothemes_queue_update( plugin_basename( __FILE__ ), '48e88957e27c7d9ceb7234d8df636261', '18625' );

// Require needed Checkout Interface Module
require_once( "Checkout_Fi.php" );
require_once( 'class-sv-wc-gateway-checkout-fi-plugin-compatibility.php' );

add_action('plugins_loaded', 'init_checkout_fi', 0);

function init_checkout_fi() {

	// Execute only if WooCommerce is enabled
	if ( ! class_exists( 'WC_Payment_Gateway' ) ) { return; }

	else {

		/**
		 * Localization
		 */
		load_plugin_textdomain('woocommerce-gateway-checkout-fi', false, dirname( plugin_basename( __FILE__ ) ) . '/i18n/languages');

		// Load some basic css styles
		add_action('wp_print_styles', 'add_checkout_stylesheet');

		function add_checkout_stylesheet() {
			$myStyleUrl = plugins_url( 'assets/css/frontend/checkout-fi.min.css', __FILE__ );
			$myStyleFile = WP_PLUGIN_DIR . '/woocommerce-gateway-checkout-fi/assets/css/frontend/checkout-fi.min.css';

			if ( file_exists( $myStyleFile ) ) {
				wp_register_style( 'myStyleSheets', $myStyleUrl );
				wp_enqueue_style( 'myStyleSheets');
			}
		}

		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'checkout_fi_action_links' );

		/**
		 * action_links function.
		 */
		function checkout_fi_action_links( $links ) {

			$plugin_links = array(
				'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=wc_gateway_checkout_fi' ) . '">' . __( 'Settings', 'woocommerce-gateway-checkout-fi' ) . '</a>',
				'<a href="http://docs.woothemes.com/document/checkout-fi/">' . __( 'Docs', 'woocommerce-gateway-checkout-fi' ) . '</a>',
				'<a href="http://support.woothemes.com/">' . __( 'Support', 'woocommerce-gateway-checkout-fi' ) . '</a>',
			);

			return array_merge( $plugin_links, $links );
		}

		/**
		 * Checkout extends default WooCommerce Payment Gateway class
		 */

		class WC_Gateway_Checkout_Fi extends WC_Payment_Gateway {

			public function __construct() {

				$this->id           = 'checkout_fi';
				$this->method_title = __( 'Checkout', 'woocommerce-gateway-checkout-fi' );
				$this->icon         = plugins_url( '/assets/images/checkout.png', __FILE__ );
				$this->has_fields   = false;

				// Load the form fields.
				$this->init_form_fields();

				// Load the settings.
				$this->init_settings();

				// Define user set variables
				$this->title                = $this->settings['title'];
				$this->description          = $this->settings['description'];
				$this->merchant_id          = $this->settings['merchant_id'];
				$this->merchant_secret      = $this->settings['merchant_secret'];
				$this->adult_entertainment  = $this->settings['adult_entertainment'];

				// Actions
				add_action( 'woocommerce_api_wc_gateway_checkout_fi', array($this, 'check_checkout_fi_response') );
				add_action('valid_checkout_fi_payment', array($this, 'successful_request') );
				add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
				add_action('woocommerce_receipt_checkout_fi', array($this, 'receipt_page'));
			}

			/**
			 * Initialise Gateway Settings Form Fields
			 */
			function init_form_fields() {

				$this->form_fields = array(
					'enabled' => array(
						'title' => __( 'Enable/Disable', 'woocommerce-gateway-checkout-fi' ),
						'type' => 'checkbox',
						'label' => __( 'Enable Checkout', 'woocommerce-gateway-checkout-fi' ),
						'default' => 'yes'
					),
					'title' => array(
						'title' => __( 'Title', 'woocommerce-gateway-checkout-fi' ),
						'type' => 'text',
						'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce-gateway-checkout-fi' ),
						'default' => __( 'Checkout', 'woocommerce-gateway-checkout-fi' )
					),
					'description' => array(
						'title' => __( 'Description', 'woocommerce-gateway-checkout-fi' ),
						'type' => 'textarea',
						'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce-gateway-checkout-fi' ),
						'default' => 'Pay via Checkout. You can pay with online bank account or with a credit card.',
					),
					'merchant_id' => array(
						'title' => __( 'Merchant ID', 'woocommerce-gateway-checkout-fi' ),
						'type' => 'text',
						'description' => __( 'Please enter your Checkout merchant id; this is needed in order to take payment.', 'woocommerce-gateway-checkout-fi' ),
						'default' => ''
					),
					'merchant_secret' => array(
						'title' => __( 'Merchant Secret', 'woocommerce-gateway-checkout-fi' ),
						'type' => 'password',
						'description' => __( 'Please enter your Checkout merchant secret; this is needed in order to take payment.', 'woocommerce-gateway-checkout-fi' ),
						'default' => ''
					),
					'adult_entertainment' => array(
						'title' => __( 'Adult Entertainment', 'woocommerce-gateway-checkout-fi' ),
						'type' => 'checkbox',
						'label' => __( 'Check this one if you are selling products that are categorized as \'adult entertainment\'.', 'woocommerce-gateway-checkout-fi' ),
						'default' => 'no'
					)
				);

			}

			/**
			 * Admin Panel Options
			 * - Options for bits like 'title' and availability on a country-by-country basis
			 *
			 * @since 1.0.0
			 */
			public function admin_options() {

				?>
				<h3><?php _e('Checkout', 'woocommerce-gateway-checkout-fi'); ?></h3>
				<p><?php _e('Checkout works by sending user to Checkout portal to enter their payment information.', 'woocommerce-gateway-checkout-fi'); ?></p>
				<table class="form-table">

				<?php if ( 'EUR' == get_woocommerce_currency() ) { ?>

					<table class="form-table">
					<?php
					// Generate the HTML For the settings form.
					$this->generate_settings_html();
					?>
					</table><!--/.form-table-->

				<?php } else { ?>

					<div class="inline error"><p><strong><?php _e( 'Gateway Disabled', 'woothemes' ); ?></strong> <?php echo sprintf( __( 'Choose Euros as your store currency in <a href="%s">General Options</a> to enable this Gateway.', 'woocommerce' ), admin_url( 'admin.php?page=wc-settings&tab=general' ) ); ?></p></div>

				<?php } // End check currency ?>

				</table><!--/.form-table-->

				<?php

			} // End admin_options()

			/**
			 * Check if this gateway is enabled and using correct currency. Only EUR allowed.
			 */
			function is_available() {

				if ( $this->enabled=="yes" ) {

				// Currency check
				if ( ! in_array(get_woocommerce_currency(), array('EUR') ) ) return false;

				// Required fields check
				if ( ! $this->merchant_id || ! $this->merchant_secret ) return false;
					return true;
				}

				return false;
			}

			/**
			 * There are no payment fields for Checkout, but we want to show the description if set.
			 */
			function payment_fields() {
				if ( $this->description ) echo wpautop( wptexturize( $this->description ) );
			}

			/**
			 * Generate the Checkout button link
			 */
			public function generate_checkout_fi_form( $order_id ) {

				$order = SV_WC_Gateway_Checkout_Fi_Plugin_Compatibility::wc_get_order( $order_id );
				$orderNumber = $order_id;
				$price = number_format( $order->get_total(), 2, '.', '' ) * 100;

				// Create new payment
				$payment = new Checkout_Fi($this->merchant_id, $this->merchant_secret);

				// Order information
				$paymentData = array();
				$paymentData["stamp"] = substr(sha1($orderNumber . time()), 0, 20);
				$paymentData["reference"] = index_number($orderNumber);
				$paymentData["message"] = sprintf(__('Order %s', 'woocommerce-gateway-checkout-fi'), $order->get_order_number() );
				$paymentData["return"] = add_query_arg('wc-api', 'wc_gateway_checkout_fi', add_query_arg(array('utm_nooverride' => '1'), $this->get_return_url( $order )));
				$paymentData["cancel"] = str_replace( '&amp;', '&', $order->get_cancel_order_url() );
				$paymentData["reject"] = $paymentData["cancel"];
				$paymentData["delayed"] = $this->get_return_url( $order );
				$paymentData["amount"] = $price; // price in cents
				$paymentData["delivery_date"] = date("Ymd");
				$paymentData["firstname"] = $order->billing_first_name;
				$paymentData["familyname"] = $order->billing_last_name;
				$paymentData["address"] = $order->billing_address_1;
				$paymentData["postcode"] = $order->billing_postcode;
				$paymentData["postoffice"] = $order->billing_city;
				$paymentData["content"] = $this->adult_entertainment == "no" ? '1' : '2';
				// XML mode with bank buttons
				$return = $payment->getCheckoutXML($paymentData);
				$xml = simplexml_load_string($return);

				// Create form with the return url to redirect client to Checkout payment page
				if( ! $xml ) {
					$post = $payment->generateCheckout($paymentData);
					$print_form = '<form action="https://payment.checkout.fi/" method="post" id="checkout_fi_payment_form">';

					foreach( $post as $field => $value ) {
						$print_form .= '<input type="hidden" name="'.$field.'" value="'.$value.'" />';
					}

					$print_form .= '<input type="submit" class="button-alt" id="submit_checkout_fi_payment_form" value="'.__('Pay via Checkout', 'woocommerce-gateway-checkout-fi').'" /> <a class="button cancel" href="'.esc_url( $order->get_cancel_order_url() ).'">'.__('Cancel order &amp; restore cart', 'woocommerce-gateway-checkout-fi').'</a>
						<script type="text/javascript">
							jQuery(function(){
								jQuery("body").block( {
									message: "<img src=\"'.esc_url( WC()->plugin_url() ).'/assets/images/ajax-loader.gif\" alt=\"Redirecting...\" style=\"float:left; margin-right: 10px;\" />'.__('Thank you for your order. We are now redirecting you to Checkout to make payment.', 'woocommerce-gateway-checkout-fi').'",
									overlayCSS: {
											background: "#fff",
											opacity: 0.6
									},
									css: {
										padding:        20,
										textAlign:      "center",
										color:          "#555",
										border:         "3px solid #aaa",
										backgroundColor:"#fff",
										cursor:         "wait",
										lineHeight:		"32px"
									}
								});
								jQuery(function(){setTimeout(function(){jQuery("#submit_checkout_fi_payment_form").click();}, 4000);});
							});
						</script>
					</form>';
					return $print_form;
				}

				// Create page with bank buttons
				elseif( $xml ) {
					$print_xml = "";

					foreach( $xml->payments->payment->banks as $bankX ) {

						foreach($bankX as $bank) {
							$print_xml .= '<div class="checkout-banks">';
							$print_xml .= '<form action="'.$bank['url'].'" method="post">';

							foreach( $bank as $key => $value ) {
								$print_xml .= '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
							}

							$print_xml .= '<span><input type="image" src="'.$bank['icon'].'" /></span>';
							$print_xml .= '<div>'.$bank['name'].'</div>';
							$print_xml .= '</form>';
							$print_xml .= '</div>';
						}

					}

					return $print_xml;
				}

				// Create error message if payment generation failed
				else {
					return '<script type="text/javascript">
						jQuery(function(){
							jQuery("body").block( {
								message: "<p>' . __("There was something wrong when processing the payment to Checkout and no transaction was made. Please contact the shop administrator if this happens again.", "checkout") .'</p><a class=\"button\" href=\"'. get_bloginfo('wpurl') .'\">' . __("Return to shop frontpage.", "checkout") . '</a>",
								overlayCSS: {
									background: "#fff",
									opacity: 0.6
								},
								css: {
									padding:         20,
									textAlign:       "center",
									color:           "#555",
									border:          "3px solid #aaa",
									backgroundColor: "#fff",
									cursor:          "pointer",
									lineHeight:      "32px"
								}
							});
						});
					</script>';
				}
			} // End generate button link

			/**
			 * Process the payment and return the result
			 */
			function process_payment( $order_id ) {

				$order = SV_WC_Gateway_Checkout_Fi_Plugin_Compatibility::wc_get_order( $order_id );

				WC()->cart->empty_cart();

				return array(
					'result'   => 'success',
					'redirect' => $order->get_checkout_payment_url( true ),
				);
			}

			/**
			 * receipt_page
			 */
			function receipt_page( $order ) {

				echo '<p>'.__('Thank you for your order, please choose your payment method.', 'woocommerce-gateway-checkout-fi').'</p>';
				echo $this->generate_checkout_fi_form( $order );
			}

			/**
			 * Check for valid response
			 */
			function check_checkout_fi_response() {
				$payment = new Checkout_Fi($this->merchant_id, $this->merchant_secret);
				$order_id = isset( $GLOBALS['wp']->query_vars['order-received'] ) ? absint( $GLOBALS['wp']->query_vars['order-received'] ) : 0;

				// If we got proper answer validate the MAC
				if( isset( $_GET['MAC'] ) ) {

					if( $payment->validateCheckout( $_GET ) ) {

						if( $payment->isPaid() ) {

							$orderdetails = array("orderid" => $order_id, "orderreference" => $_GET['REFERENCE'], "ordercheckout" => $_GET['PAYMENT']);

							// If payment is pending
							if( $_GET['STATUS'] == "5" ) {
								$order = SV_WC_Gateway_Checkout_Fi_Plugin_Compatibility::wc_get_order( $order_id );
								$order->update_status('on-hold', sprintf(__('Order payment via Checkout is pending confirmation.', 'woocommerce-gateway-checkout-fi')));
							}

							// If payment is done
							else {
								do_action("valid_checkout_fi_payment", $orderdetails);
								$order = SV_WC_Gateway_Checkout_Fi_Plugin_Compatibility::wc_get_order( $order_id );
								wp_redirect( $this->get_return_url( $order ) );
								exit;
							}
						}

						// Payment failed
						else {
							$order = SV_WC_Gateway_Checkout_Fi_Plugin_Compatibility::wc_get_order( $order_id );
							$order->update_status('failed', sprintf(__('Checkout payment failed. Payment Checkout archive record is %s.', 'woocommerce-gateway-checkout-fi'), strtolower($_GET['PAYMENT']) ) );
						}
					}

					// Payment failed and there was possibly attempt to bypass actual payment
					else {
						$order = SV_WC_Gateway_Checkout_Fi_Plugin_Compatibility::wc_get_order( $order_id );
						$order->update_status('failed', sprintf(__('Checkout payment failed. Possible hacking attempt. Payment Checkout archive record is %s.', 'woocommerce-gateway-checkout-fi'), strtolower($_GET['PAYMENT']) ) );
					}
				}

			}

			/**
			 * Successful Payment!
			 */
			function successful_request( $orderdetails ) {

				$order = SV_WC_Gateway_Checkout_Fi_Plugin_Compatibility::wc_get_order( $orderdetails['orderid'] );
				$order->payment_complete();
				$order->add_order_note( sprintf(__('Checkout payment complete. Payment reference is %s. Payment Checkout archive record is %s.', 'woocommerce-gateway-checkout-fi'), strtolower($orderdetails['orderreference']), strtolower($orderdetails['ordercheckout']) ) );
			}


		}


		/**
		 * Function for generating correctly formed index number (viitenumero)
		 */
		function index_number( $order_number ) {

			if( strlen($order_number) == 1 ) $order_number = "00" . $order_number;

			if( strlen( $order_number ) == 2 ) $order_number = "0" . $order_number;

			if( strlen( $order_number ) > 19 ) return 0;

			$factors = array( '7', '3', '1', '7', '3', '1', '7', '3', '1', '7', '3', '1', '7', '3', '1', '7', '3', '1', '7' );
			$checksum = 0;
			$j = 0;
			$tmp = $order_number;
			settype( $tmp, "string" );

			for( $i = strlen( $tmp ) - 1; $i > -1; $i-- ) {
				$checksum = $checksum + $factors[$j++] * intval(substr($tmp, $i, 1));
			}

			$checksum = ceil( $checksum / 10 ) * 10 - $checksum;
			$index = "$order_number$checksum";

			return $index;
		}

		/**
		 * Add the gateway to WooCommerce
		 */
		function add_checkout_fi_gateway( $methods ) {
			$methods[] = 'WC_Gateway_Checkout_Fi'; return $methods;
		}

		add_filter('woocommerce_payment_gateways', 'add_checkout_fi_gateway' );

	}

}
