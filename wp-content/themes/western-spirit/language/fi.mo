��    D      <  a   \      �     �     �      !     3   >     r     ~     �     �     �     �     �     �     �  
   �     �     �          /  	   A  
   K     V     ^     d     r     �     �     �     �  )   �     �     �     �     �  	   	  3   	     B	     V	     ^	     t	     }	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     
     +
      4
     U
     d
     j
     x
  
   }
  I   �
     �
     �
     �
  '        )     /     <     A  �  R     1     B    ]  (   m  8   �     �     �     �     �        
             -  	   E  
   O  	   Z     d  +   �     �     �     �     �     �     �               *     6     J  1   O     �     �     �     �     �  7   �     �               ,     5     Q     b     n     �     �     �     �     �     �     �  	   �     �                          &  S   4     �  !   �     �  1   �     �     �  
                         6                 .      (   /   0   =      $           4                   #          C   3   B      *       D      ?           2   A       +   !   >      &             ;   ,   7       5                             )                 	   @   
   <            9   1       "                            8   -   :      %   '                         &larr; Previous Entries (Pro Edition Only) <div class='config-error'><h2>PHP Version Problem</h2>Looks like you are using PHP version: <strong>%s</strong>. To run this framework you will need PHP <strong>5.0</strong> or better...<br/><br/> Don't worry though! Just check with your host about a quick upgrade.</div> <h2>Unable to fetch from API</h2> <span class='meta-nav'>&larr;</span> Older Comments Add Comment Add New  Allowed Tags Archives Author Background Image Background Image Settings Background Repeat Calendar Categories Comments Comments are closed. DMS Header Scripts Fallback DMS LESS Fallback Dashboard Deactivate Default Edit  Entries (RSS) Export DMS Config Fit image to page? Fixed Global Settings Go Horizontal Background Position in Percent Import DMS Config Media Library Meta More More Info Newer Comments <span class='meta-nav'>&rarr;</span> Next Entries &rarr; Page %s Page Background Image Page: %s PageLines Forum PageLines Store Read More Read More &raquo; Recent Scroll Search Search Results: %s Select Layout Mode Select This Image For Option Set Background Attachment Settings Sorry, This Page Does not exist. Stay in Touch! Store Tagged With:  Tags The Latest This post is password protected. Enter the password to view any comments. Type:  Unsupported file type! Upload Error: %s Vertical Background Position in Percent View  Your Account home or try a search? Project-Id-Version: PageLines DMS
POT-Creation-Date: 2013-11-05 18:32-0000
PO-Revision-Date: 2014-10-28 15:28+0200
Last-Translator: ketri <petripottonen@gmail.com>
Language-Team: Finnish (Finland) (http://www.transifex.com/projects/p/pagelines-dms/language/fi_FI/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi_FI
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.6.9
X-Poedit-Basepath: .
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;lang
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 &larr; Edelliset (Ainoastaan Pro-versiossa) <div class='config-error'><h2>PHP-versio-ongelma</h2>Näyttää siltä, että käytät PHP-versiota <strong>%s</strong>. Käyttääksesi DMS:ää tarvitset version <strong>5.0</strong> tai paremman.<br/><br/> Pyydä palveluntarjoajaa päivittämään ohjelmistonsa.</div> <h2>API:sta noutaminen epäonnistui</h2> <span class='meta-nav'>&larr;</span> Vanhemmat kommentit Lisää kommentti Lisää Sallitut tagit Arkisto Kirjoittaja Taustakuva Taustakuvan asetukset Taustakuvan toistaminen Kalenteri Kategoriat Kommentit Kommentointi pois päältä. DMS Headerin scriptien hätävaravaihtoehto DMS LESS hätävaravaihtoehto Ohjauspaneeli Epäaktivoi Oletus Muokkaa Artikkelit (RSS) Vie DMS config Mahduta kuva sivulle? Kiinnitetty Globaalit asetukset Mene Vaakasuuntainen taustakuvan sijainti prosentteina Tuo DMS Config Mediakirjasto Meta-tiedot Lisää Lisätietoja Uudemmat kommentit <span class='meta-nav'>&rarr;</span> Seuraavat &rarr; Sivu %s Sivun taustakuvan asetukset Sivu: %s Pagelines keskustelufoorumi Pagelines-kauppa Lue lisää Lue lisää &raquo; Uusimmat Vieritä Haku Haun tulokset: %s Valitse  Valitse tämä kuva Aseta liite taustakuvaksi Asetukset Tätä sivua ei ole. Pidä yhteyttä! Kauppa Tagit: Tagit Viimeisimmät Tämä artikkeli on suojattu salasanalla. Kirjoita salasana nähdäksesi kommentit. Tyyppi: Tätä tiedostotyyppiä ei tueta! Latausvirhe: %s Pystysuuntainen taustakuvan sijainti prosentteina Katso Käyttäjätilisi etusivulle tai kokeile hakua? 